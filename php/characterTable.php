<?php

        $host = "localhost";
        $userName = "D00116603";
        $userPass = ""; //SHA1()
        $dbname = "D00116603";
        
        try
        {
            $pdo = new PDO("mysql:dbname=$dbname;host=$host",
                    $userName, $userPass);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = $query = "CREATE TABLE characters (id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
                               name VARCHAR(30) NOT NULL,
                               age INT,
                               birth VARCHAR(50),
                               appearance VARCHAR(30),
                               affiliation VARCHAR(30),
                               pic VARCHAR(600)
                              )";

            $pdo->exec($sql);
            unset($pdo);
            echo "Table created successfully!<br>";
        }
        catch(PDOException $e)
        {
            echo "Create Table: " . $e->getMessage();
        }