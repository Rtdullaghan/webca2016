<!DOCTYPE html>
<html>
    <head>
        <title>All Records</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/database.css">
    </head>

        <div id="mainBox">
            <div id="heading">
                <img id="heading_image" src="../assets/images/databaseHeader.png"  alt="banner" />
            </div>
            
             <div id="navBarBox">
                <ul>
                        <li><a class="active " href="../index.html">Home</a></li>
                        <li><a href="insert.php">Add Record</a></li>
                        <li><a href="update.php">Alter Record</a></li>
                        <li><a href="delete.php">Delete Record</a></li>
                        <li><a href="search.php">Search</a></li>
                        
                </ul>
            </div>

            <div id="mainArea">
                    <?php
                    /* Validate and assign input data */
                    /* As displaying all records does not require any input from the calling webpage, we do not need any input values */


                    /* Set $isAdministrator to true to turn on the administrator functionality */
                    /* Set to false to turn off */
                    $isAdministrator = true; // set to false if this is not the administrator



                    /* Include "configuration.php" file */
                    require_once "configuration.php";



                    /* Connect to the database */
                    $dbConnection = new PDO("mysql:host=$dbHost;dbname=$dbName", $dbUsername, $dbPassword);
                    $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   // set the PDO error mode to exception



                    /* Perform query */
                    $query = "SELECT id, name, age, birth, appearance, affiliation, pic FROM characters";
                    $statement = $dbConnection->prepare($query);
                    $statement->execute();



                    /* Manipulate the query result */
                    if ($statement->rowCount() > 0) {
                        echo "<table>";
                        echo "<tr>";
                        echo "<th> Name </th>";
                        echo "<th> Age </th>";
                        echo "<th> Birth Place </th>";
                        echo "<th> Appearance </th>";
                        echo "<th> Affiliation </th>";
                        echo "<th> Image </th>";
                        echo "<th> ID No. </th>";
                        echo "<th> Option 1 </th>";
                        echo "<th> Option 2 </th>";
                        echo "</tr>";
                        $result = $statement->fetchAll(PDO::FETCH_OBJ);
                        foreach ($result as $row) {
                            echo "<tr>";
                            echo "<td>" . $row->name . "</td><td>" . $row->age . "</td><td>" . $row->birth . "</td><td>" . $row->appearance . "</td><td>" . $row->affiliation . "</td><td>" . $row->pic . "</td><td>" . $row->id . "</td>";
                            if ($isAdministrator) {
                                echo "<td><a href='update.php?id=" . $row->id .
                                "&name=" . $row->name .
                                "&age=" . $row->age .
                                "&birth=" . $row->birth .
                                "&appearance=" . $row->appearance .
                                "&affiliation=" . $row->affiliation .
                                "&pic=" . $row->pic . "'>edit</a></td>" .
                                "<td><a href='javascript:deleteRecord(" . $row->id . ")'>delete</a></td>";
                            }
                            echo "</tr>";
                        }
                        echo "</table>";
                    }
                    echo "<p id='record'>" . $statement->rowCount() . " records found.</p>";



                    /* Provide a link for the user to proceed to a new webpage or automatically redirect to a new webpage */
                    if ($isAdministrator) {
                        echo "<form action='insert.php'>";
                        echo "<input type ='submit' value ='Add New Record'>";
                        echo "</form>";
                    }
                    ?> 

                    <form id = 'deleteRecord' action = 'deleteTransaction.php' method = 'post'>
                        <input type = 'hidden' id = 'id' name = 'id'>
                    </form>

                    <script>
                        function deleteRecord(id)
                        {
                            document.getElementById('id').value = id.toString();
                            document.getElementById('deleteRecord').submit();
                        }
                    </script>

            </div>

            <div id="footer">
                <img id="footerImage" src="../assets/images/databaseFooter.png"  alt="footer" />
            </div>
        </div>
    </body>
</html>