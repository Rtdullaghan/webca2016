<!DOCTYPE html>
<html>
    <head>
        <title>All Records</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/database.css">
    </head>

        <div id="mainBox">
            <div id="heading">
                <img id="heading_image" src="../assets/images/databaseHeader.png"  alt="banner" />
            </div>
            
             <div id="navBarBox">
                <ul>
                        <li><a class="active " href="../index.html">Home</a></li>
                        <li><a href="displayAllRecords.php">Records</a></li>
                        <li><a href="insert.php">Add Record</a></li>
                        <li><a href="update.php">Alter Record</a></li>
                        <li><a href="delete.php">Delete Record</a></li>
                        <li><a href="search.php">Search</a></li>
                </ul>
            </div>
            <div id="mainArea">  
<?php
                    /* Validate and assign input data */
                    $name = ltrim(rtrim(filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING)));
                    if (empty($name)) {
                        header("location: search.php"); // deal with invalid input
                        exit();
                    }



                    /* Include "configuration.php" file */
                    require_once "configuration.php";



                    /* Perform Query */
                    $query = "SELECT id, name, age, birth, appearance, affiliation, pic FROM characters WHERE name = :name";
                    $statement = $dbConnection->prepare($query);
                    $statement->bindParam(":name", $name, PDO::PARAM_STR);
                    $statement->execute();



                    /* Manipulate the query result */
                    if ($statement->rowCount() > 0) {
                        echo "<table>";
                        echo "<tr>";
                        echo "<th> Name </th>";
                        echo "<th> Age </th>";
                        echo "<th> Birth Place </th>";
                        echo "<th> Appearance </th>";
                        echo "<th> Affiliation </th>";
                        echo "<th> Image </th>";
                        echo "<th> ID No. </th>";
                        echo "</tr>";
                        $result = $statement->fetchAll(PDO::FETCH_OBJ);
                        foreach ($result as $row) {
                            echo "<tr>";
                            echo "<td>" . $row->name . "</td><td>" . $row->age . "</td><td>" . $row->birth . "</td><td>" . $row->appearance . "</td><td>" . $row->affiliation . "</td><td>" . $row->pic . "</td><td>" . $row->id . "</td>";
                            echo "</tr>";
                        }
                        echo "</table>";
                    }
                    echo "<p>" . $statement->rowCount() . " records found.</p>";



                    /* Provide a link for the user to proceed to a new webpage or automatically redirect to a new webpage */
                    /* We could provide a link to search.php using the code below */
                    echo "<a href='search.php'>Perform another search</a>";
                    ?>
         </div>
                <div id="footer">
                        <img id="footerImage" src="../assets/images/databaseFooter.png"  alt="footer" />
                </div>
        </div>
    </body>
</html>