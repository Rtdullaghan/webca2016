<!DOCTYPE html>
<html>
    <head>
        <title>Rebel Alliance</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/rebel.css">
        <script src="../js/defect.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
    </head>
    <body onload="rebelPageChanges();">
        <div id="mainBox">
            <div id="heading">
                 <img id="headingImage" src="../assets/images/rebelHeader.png"  alt="banner" />                 
            </div>
            
            <div id="navBarBox">
                <ul>
                        <li><a class="active " href="../index.html">Home</a></li>
                        <li><a href="displayAllRecords.php">Records</a></li>
                        <li><a href="insert.php">Add Record</a></li>
                        <li><a href="update.php">Alter Record</a></li>
                        <li><a href="delete.php">Delete Record</a></li>
                        <li><a href="search.php">Search</a></li>
                        <li><a id="rebelDefect" href="imperialHome.php">DEFECT</a></li>
                </ul>
            </div>
         <div id="mainArea">
                        <h1>Intro</h1>
             
            <img class="alignRight" id="rightAlign" alt="bail" src="../assets/images/bailOrgana.jpg">  
            <p>The Alliance to Restore the Republic is a military resistance movement, controlled by the Alliance High Command and dedicated to upholding the ideals of the Old Republic. 
                We frequently change our base of operations, ranging from numerous worlds across the galaxy in order to avoid detection or regroup from an attack by our nemesis, the Galactic Empire. 
                The Alliance Diplomatic Corps seek to maintain friendly relations and procure support from various worlds in the galaxy to help our cause. Despite this, the Alliance has few worlds openly declaring their 
                support for the movement, while those few that do find their worlds cordoned off behind Imperial blockades</p>
             
             <p>The Rebel Alliance is a resistance group created by Bail Organa and Mon Mothma to stop the reign of the Galactic Empire.
             Before the creation of this Alliance there was a smaller movement set up by Organa. These rebels came together from a variety of different cells such as the crew of the Ghost and Phoenix Squadron.
             The Rebel Alliance was born from hatred of the current Empire. They wished to restore the old Republic. It was led by renegade senate members such as Leia Organa and is made up of citizens as well
             as defectors from the Empires military forces.</p>
            
             <p>The Rebel Alliance is a resistance group created by Bail Organa and Mon Mothma to stop the reign of the Galactic Empire.
             Before the creation of this Alliance there was a smaller movement set up by Organa. These rebels came together from a variety of different cells such as the crew of the Ghost and Phoenix Squadron.
             The Rebel Alliance was born from hatred of the current Empire. They wished to restore the old Republic. It was led by renegade senate members such as Leia Organa and is made up of citizens as well
             as defectors from the Empires military forces.</p>
             
             <img class="alignLeft" id="leftAlign" alt="leia" src="../assets/images/leia.jpg">
             
             <p>After successfully stealing the plans for the Empire's ultimate weapon, the Death Star, and destroying the space station in the Battle of Yavin with the help of Luke Skywalker, the tide of the war began to
                 change and our cause began to prove a severe thorn in the Empire's side, and we continue to engage in battles with the Empire. 
                 However, under the command of Darth Vader, the Imperial forces succeeded in defeating the Alliance on Hoth, forcing them to scatter their forces in order to survive.</p>
             
             <p>Eventually,we prevailed against the Empire and critically damaged them when we launched an attack against the second Death Star during its construction in what we call the Battle of Endor. Not only did 
                 we manage to destroy the super weapon, but the Imperial Navy was severely crippled by the loss of one of their Super Star Destroyers.</p>
             
         </div>
                <div id="footer">
                        <img id="footerImage" src="../assets/images/rebelFooter.png"  alt="footer" />
                </div>
        </div>
    </body>
</html>