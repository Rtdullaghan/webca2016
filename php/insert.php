<!DOCTYPE html>
<html>
    <head>
        <title>All Records</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/database.css">
    </head>

        <div id="mainBox">
            <div id="heading">
                <img id="heading_image" src="../assets/images/databaseHeader.png"  alt="banner" />
            </div>
            
             <div id="navBarBox">
                <ul>
                        <li><a class="active " href="../index.html">Home</a></li>
                        <li><a href="displayAllRecords.php">Records</a></li>
                        <li><a href="update.php">Alter Record</a></li>
                        <li><a href="delete.php">Delete Record</a></li>
                        <li><a href="search.php">Search</a></li>
                </ul>
            </div>
            <div id="mainArea">
             <form action="insertTransaction.php" method="post">
            <fieldset>
            <legend>
                Enter Details:
            </legend>
                <label for="name">Name: </label>
                <input type="text" id = "name" name = "name" required><br>
                <label for="age">Age: </label>
                <input type="number" id = "age" name = "age" required><br>
                <label for="birth">Birth Place: </label>
                <input type="text" id = "birth" name = "birth"><br>
                <label for="appearance">Appearance: </label>
                <input type="text" id = "appearance" name = "appearance" required><br>
                <label for="affiliation">Affiliation: </label>
                <input type="text" id = "affiliation" name = "affiliation" required><br>  
                <label for="pic">Image: </label>
                <input type="text" id = "pic" name = "pic"><br> 
            </fieldset>
            <input type="submit" value="Add Record">
            </form>
         </div>
                <div id="footer">
                        <img id="footerImage" src="../assets/images/databaseFooter.png"  alt="footer" />
                </div>
        </div>
    </body>   
</html>