<!DOCTYPE html>
<html>
    <head>
        <title>All Records</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/database.css">
        <script src="../js/updateFunctions.js"></script>
        
    </head>

        <div id="mainBox">
            <div id="heading">
                <img id="heading_image" src="../assets/images/databaseHeader.png"  alt="banner" />
            </div>
            
             <div id="navBarBox">
                <ul>
                        <li><a class="active " href="../index.html">Home</a></li>
                        <li><a href="displayAllRecords.php">Records</a></li>
                        <li><a href="insert.php">Add Record</a></li>
                        <li><a href="update.php">Alter Record</a></li>
                        <li><a href="delete.php">Delete Record</a></li>
                </ul>
            </div> 
<div id="mainArea">

             <form action="searchTransaction.php" method="post">
            <fieldset>
                <legend>
                    Enter name:
                </legend>
            <label for="name">Name: </label>
            <input type="text" id = "name" name = "name" required><br>
            </fieldset>
            <input type="submit" value="Search">
            </form>
             
             <p> OR </p>
             
             <form action="searchNameAndAffliationTransaction.php" method="post">
            <fieldset>
            <legend>
                Enter name and affiliation:
            </legend>
            <label for="name">Name: </label>
            <input type="text" id = "name" name = "name"><br>
            <label for="affiliation">Affiliation: </label>
            <input type="text" id = "affiliation" name = "affiliation"><br>
            </fieldset>        
            <input type="submit" value="Search">
            </form>
             
         </div>
            
                <div id="footer">
                        <img id="footerImage" src="../assets/images/databaseFooter.png"  alt="footer" />
                </div>
        </div>
    </body>
</html>
