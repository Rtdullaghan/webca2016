<!DOCTYPE html>
<html>
    <head>
        <title>All Records</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/database.css">
        <script src="../js/updateFunctions.js"></script>
        
    </head>

        <div id="mainBox">
            <div id="heading">
                <img id="heading_image" src="../assets/images/databaseHeader.png"  alt="banner" />
            </div>
            
             <div id="navBarBox">
                <ul>
                        <li><a class="active " href="../index.html">Home</a></li>
                        <li><a href="displayAllRecords.php">Records</a></li>
                        <li><a href="insert.php">Add Record</a></li>
                        <li><a href="update.php">Alter Record</a></li>
                        <li><a href="search.php">Search</a></li>
                </ul>
            </div> 
<div id="mainArea">

                    <form action="deleteTransaction.php" method="post">
                        <fieldset>
                            <legend>
                                Enter ID No. to delete entry:
                            </legend>
                        <label for="id">Id:  </label>
                        <input type="text" id = "id" name = "id" required><br>
                        </fieldset>
                        <input type="submit" value="Delete Record">
                    </form>

            </div>

            <div id="footer">
                <img id="footerImage" src="../assets/images/databaseFooter.png"  alt="footer" />
            </div>
        </div>

        <script>
            if (getURLValue('id') !== null)
            {
                document.getElementById('id').value = getURLValue('id');
            }
        </script>

    </body>
</html>