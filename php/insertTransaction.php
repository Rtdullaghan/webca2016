<!DOCTYPE html>
<htm<!DOCTYPE html>
l>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Insert</title>
</head>
<body>

<?php
/* Validate and assign input data */
$name = ltrim(rtrim(filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING)));
if (empty($name))
{
    header("location: insert.php"); // deal with invalid input
    exit();
}

$age = ltrim(rtrim(filter_input(INPUT_POST, "age", FILTER_SANITIZE_NUMBER_INT)));
if ((empty($age)) || (!filter_var($age, FILTER_VALIDATE_INT)))
{
    header("location: insert.php"); // deal with invalid input
    exit();
}

$birth = ltrim(rtrim(filter_input(INPUT_POST, "birth", FILTER_SANITIZE_STRING)));
if (empty($birth)) 
{
    header("location: insert.php"); // deal with invalid input
    exit();
}

$appearance = ltrim(rtrim(filter_input(INPUT_POST, "appearance", FILTER_SANITIZE_STRING)));
if (empty($appearance)) 
{
    header("location: insert.php");
    exit();
}

$affiliation = ltrim(rtrim(filter_input(INPUT_POST, "affiliation", FILTER_SANITIZE_STRING)));
if (empty($affiliation)) 
{
    header("location: insert.php");
    exit();
}

$pic = ltrim(rtrim(filter_input(INPUT_POST, "pic")));
if (empty($pic)) 
{
       header("location: insert.php");
      exit();
}





/* Include "configuration.php" file */
require_once "configuration.php";



/* Perform Query */
$query = "INSERT INTO characters (name, age, birth, appearance, affiliation, pic) VALUES(:name, :age, :birth, :appearance, :affiliation, :pic)";
$statement = $dbConnection->prepare($query);
$statement->bindParam(":name", $name, PDO::PARAM_STR);
$statement->bindParam(":age", $age, PDO::PARAM_INT);
$statement->bindParam(":birth", $birth, PDO::PARAM_STR);
$statement->bindParam(":appearance", $appearance, PDO::PARAM_STR);
$statement->bindParam(":affiliation", $affiliation, PDO::PARAM_STR);
$statement->bindParam(":pic", $pic, PDO::PARAM_STR);
$statement->execute();



/* Provide feedback that the record has been added */
if ($statement->rowCount() > 0)
{
    echo "<p>Record successfully added to database.</p>";
}
else
{
    echo "<p>Record not added to database.</p>";
}



/* Provide a link for the user to proceed to a new webpage or automatically redirect to a new webpage */
header("location: " . $siteName . "/php/displayAllRecords.php");
?>

</body>
</html>

