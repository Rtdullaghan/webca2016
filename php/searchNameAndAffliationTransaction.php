<!DOCTYPE html>
<html>
    <head>
        <title>All Records</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/database.css">
    </head>

        <div id="mainBox">
            <div id="heading">
                <img id="heading_image" src="../assets/images/databaseHeader.png"  alt="banner" />
            </div>
            
             <div id="navBarBox">
                <ul>
                        <li><a class="active " href="../index.html">Home</a></li>
                        <li><a href="displayAllRecords.php">Records</a></li>
                        <li><a href="insert.php">Add Record</a></li>
                        <li><a href="update.php">Alter Record</a></li>
                        <li><a href="delete.php">Delete Record</a></li>
                        <li><a href="search.php">Search</a></li>
                </ul>
            </div>
            <div id="mainArea"> 
<?php
        /* Validate and assign input data */
        /* Note that, in this example, both model and colour are allowed to be empty */
        $name = filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING);
        $affiliation = filter_input(INPUT_POST, "affiliation", FILTER_SANITIZE_STRING);



        /* Include "configuration.php" file */
        require_once "configuration.php";



        /* Perform Query */
        if (empty($name) && empty($affiliation)) {
            $query = "SELECT id, name, age, birth, appearance, affiliation, pic FROM characters";
            $statement = $dbConnection->prepare($query);
        } else {
            if (empty($name)) { // appear is not empty
                $query = "SELECT id, name, age, birth, appearance, affiliation, pic FROM characters WHERE affiliation = :affiliation";
                $statement = $dbConnection->prepare($query);
                $statement->bindParam(":affiliation", $affiliation, PDO::PARAM_STR);
            } else if (empty($affiliation)) { // name is not empty
                $query = "SELECT id, name, age, birth, appearance, affiliation, pic FROM characters WHERE name = :name";
                $statement = $dbConnection->prepare($query);
                $statement->bindParam(":name", $name, PDO::PARAM_STR);
            } else {
                $query = "SELECT id, name, age, birth, appearance, affiliation, pic FROM characters WHERE name = :name AND affiliation = :affiliation";
                $statement = $dbConnection->prepare($query);
                $statement->bindParam(":name", $name, PDO::PARAM_STR);
                $statement->bindParam(":affiliation", $affiliation, PDO::PARAM_STR);
            }
        }
        $statement->execute();



        /* Manipulate the query result */
        if ($statement->rowCount() > 0) {
            echo "<table>";
            echo "<tr>";
            echo "<th> Name </th>";
            echo "<th> Age </th>";
            echo "<th> Height </th>";
            echo "<th> Appearance </th>";
            echo "<th> Affiliation </th>";
            echo "<th> Image </th>";
            echo "<th> ID No. </th>";
            echo "</tr>";
            $result = $statement->fetchAll(PDO::FETCH_OBJ);
            foreach ($result as $row) {
                echo "<tr>";
                echo "<td>" . $row->name . "</td><td>" . $row->age . "</td><td>" . $row->birth . "</td><td>" . $row->appearance . "</td><td>" . $row->affiliation . "</td><td>" . $row->pic . "</td><td>" . $row->id . "</td>";
                echo "</tr>";
            }
            echo "</table>";
        }
        echo "<p>" . $statement->rowCount() . " records found.</p>";



        /* Provide a link for the user to proceed to a new webpage or automatically redirect to a new webpage */
        echo "<a href='search.php'>Perform another search</a>";
        ?> 
         </div>
                <div id="footer">
                        <img id="footerImage" src="../assets/images/databaseFooter.png"  alt="footer" />
                </div>
        </div>
    </body>
</html>