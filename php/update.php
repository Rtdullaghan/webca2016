<?php
require_once 'upload.php';
if(isset($_POST['SubmitButton'])){
require_once '../upload.php';
if ($_FILES['fileToUpload']['tmp_name'] != "")
    {

    $image = $_FILES['fileToUpload']['tmp_name'];// then sit variable image to be the image posted from form

    if (checkImageValid($image))
    {
       $toSend = uploadImage($image);// then sends it to destination via uploadImage function
    }
    }
}
if(isset($_POST['SubmitButton'])){
    
    
}


?>
<html>
    <head>
        <title>All Records</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/database.css">
        <script src="../js/updateFunctions.js"></script>
        <script src="../upload.php"></script>
        
    </head>

        <div id="mainBox">
            <div id="heading">
                <img id="heading_image" src="../assets/images/databaseHeader.png"  alt="banner" />
            </div>
            
             <div id="navBarBox">
                <ul>
                        <li><a class="active " href="../index.html">Home</a></li>
                        <li><a href="displayAllRecords.php">Records</a></li>
                        <li><a href="insert.php">Add Record</a></li>
                        <li><a href="delete.php">Delete Record</a></li>
                        <li><a href="search.php">Search</a></li>
                </ul>
            </div>
            <div id="mainArea">
                <fieldset>
                <form action="#" method="post" enctype="multipart/form-data">
                        Select image to upload:
                        <input type="file" name="fileToUpload" id="fileToUpload" accept="image/*">
                        <input type="submit" value="Upload Image" name="submit" > 
                </form>
                <form action="update" method="post">
                    
                        <legend>
                            Enter new info:
                        </legend>
                        
                        <label for="id">Id: </label>
                        <input type="number" id = "id" name = "id" required><br>
                        <label for="name">Name: </label>
                        <input type="text" id = "name" name = "name" required><br>
                        <label for="age">Age: </label>
                        <input type="number" id = "age" name = "age" required><br>
                        <label for="birth">Birth Place: </label>
                        <input type="text" id = "birth" name = "birth"><br>
                        <label for="appearance">Appearance: </label>
                        <input type="text" id = "appearance" name = "appearance" required><br> 
                        <label for="affiliation">Affiliation: </label>
                        <input type="text" id = "affiliation" name = "affiliation" required><br> 
                        <label for="pic">Image: </label>
                        <input type="text" id="pic" name="pic" required/><br>
                    </fieldset>
                    <input type="submit" value="Update Record">
                </form>
            </div>
            <div id="footer">
                <img id="footerImage" src="../assets/images/databaseFooter.png"  alt="footer" />
            </div>
        </div>

        <script>
           
            if (getURLValue('id') !== null)
            {
                document.getElementById('id').value = getURLValue('id');
                document.getElementById('name').value = getURLValue('name');
                document.getElementById('age').value = getURLValue('age');
                document.getElementById('birth').value = getURLValue('birth');
                document.getElementById('appearance').value = getURLValue('appearance');
                document.getElementById('pic').value = getURLValue('pic');
            }
        </script>

    </body>
</html>
