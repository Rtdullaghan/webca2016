<!DOCTYPE html>
<html>
    <head>
        <title>Imperial Army</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/imperial.css">
        <script src="../js/defect.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet">
    </head>

    <body onload="imperialPageChanges();">
        <div id="mainBox">
            <div id="heading">
                 <img id="headingImage" src="../assets/images/imperialHeader.png"  alt="banner" />
            </div>
            
            <div id="navBarBox">
                <ul>
                        <li><a class="active " href="../index.html">Home</a></li>
                        <li><a href="displayAllRecords.php">Records</a></li>
                        <li><a href="insert.php">Add Record</a></li>
                        <li><a href="update.php">Alter Record</a></li>
                        <li><a href="delete.php">Delete Record</a></li>
                        <li><a href="search.php">Search</a></li> <li><a id ="imperialDefect"  href="rebelHome.php">DEFECT</a></li>
                </ul>
            </div>
         <div id="mainArea">
           
                        <h1>Intro</h1>
                       
            <img class="alignRight" id="rightAlign" alt="vader" src="../assets/images/darthVaderPortrait.jpg">  
            
            <p>The Empire rose to power in the final years of the Republic. The Sith Lord Darth Sidious slowly secured the government under his control through the persona of Sheev Palpatine, the last Supreme Chancellor of the Republic. 
                Through his schemes as both Sidious and Palpatine, the Sith rose once again to a position of dominance amidst the collapse of the Jedi Order and the marginalization of the Galactic Senate. Without the Jedi of the light side to oppose him, 
                Sidious brought an end to a thousand years of democracy and in its place he declared himself the Galactic Emperor.</p>
             
             <p>For over twenty years any opposition to the Imperial regime was squashed by the rapidly expanding armed forces of the Empire. Throughout most of the Empire's existence, the Imperial Senate stood within the Empire as the last surviving symbol 
                 of the Republic. With the completion of the Death Star, the Emperor felt confident enough to overthrow the body of galactic representatives while maintaining his control over the galaxy. However, the growing resentment to Imperial rule slowly led to the 
                 rise of various resistance movements that ultimately culminated in the formation of the Alliance to Restore the Republic. United by a common goal to destroy the Empire the Alliance became a viable threat to the Empire after its successful theft of the Death 
                 Star plans and the station's subsequent destruction at the Battle of Yavin.</p>
            
             <p>Throughout the Galactic Civil War, Imperial and Alliance forces fought across the galaxy in a war that resulted in the deaths of Darth Sidious and his understudy Darth Vader. With the Sith destoyed the Grand Vizier of the Imperial Ruling Council, Mas Amedda, 
                 succeeded Palpatine as Emperor but held limited power beyond Coruscant as the Empire fully evolved into a stratocracy dominated by military officers. It was during this time when the Empire split into a variety of separate factions, dominated by various warlords, 
                 moffs, and admirals, while the loyalist Imperial government remained on Coruscant.</p>
             
             <img class="alignLeft" id="leftAlign" alt="maul" src="../assets/images/DarthMaul.jpg">
             
             <p>After successfully stealing the plans for the Empire's ultimate weapon, the Death Star, and destroying the space station in the Battle of Yavin with the help of Luke Skywalker, the tide of the war began to
                 change and our cause began to prove a severe thorn in the Empire's side, and we continue to engage in battles with the Empire. 
                 However, under the command of Darth Vader, the Imperial forces succeeded in defeating the Alliance on Hoth, forcing them to scatter their forces in order to survive.</p>
             
             <p>Eventually,we prevailed against the Empire and critically damaged them when we launched an attack against the second Death Star during its construction in what we call the Battle of Endor. Not only did 
                 we manage to destroy the super weapon.</p>
             
         </div>
                <div id="footer">
                        <img id="footerImage" src="../assets/images/imperialFooter.png"  alt="footer" />
                </div>
        </div>
    </body>
</html>