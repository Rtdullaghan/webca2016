<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>PHP Create Database Table</title>
</head>
<body>

<?php
/* Include "configuration.php" file */
require_once "configuration.php";



/* Create the database */
$query = "CREATE DATABASE IF NOT EXISTS $dbName";
$statement = $dbConnection->prepare($query);
$statement->execute();



/* Provide feedback to the user */
echo "Database '$dbName' created.";
?>