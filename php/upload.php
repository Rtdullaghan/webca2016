<?php

const MAX_FILE_SIZE = 500000; 

function checkImageValid($image) 
{
    $valid = true;
    $size = filesize($image);// gets size of image
    if ($size > MAX_FILE_SIZE)// compares to max size defined
    {
        $valid = false;
        echo 'The image You are trying to upload is too large';
    }

    return $valid;// will be true of image is smaller than max file size
}

function uploadImage($image) {
    
    $imageName =  "Original.jpg";
    $destFile = "../assets/databaseImages/" . $imageName;
    move_uploaded_file($image, $destFile);//moves image to new location
    $fileDest = '<img id="db_img" src="' . $destFile . '"/>';
    return $fileDest;
    
}

