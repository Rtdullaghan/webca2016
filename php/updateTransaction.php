<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>update</title>
</head>
<body>

<?php
/* Validate and assign input data */
$id = ltrim(rtrim(filter_input(INPUT_POST, "id", FILTER_SANITIZE_NUMBER_INT)));
if ((empty($id)) || (!filter_var($id, FILTER_VALIDATE_INT)))
{
    header("location: update.php"); // deal with invalid input
    exit();
}

$name = ltrim(rtrim(filter_input(INPUT_POST, "name", FILTER_SANITIZE_STRING)));
if (empty($name))
{
    header("location: update.php"); // deal with invalid input
    exit();
}

$age = ltrim(rtrim(filter_input(INPUT_POST, "age", FILTER_SANITIZE_NUMBER_INT)));
if ((empty($age)) || (!filter_var($age, FILTER_VALIDATE_INT)))
{
    header("location: update.php"); // deal with invalid input
    exit();
}

$birth = ltrim(rtrim(filter_input(INPUT_POST, "birth", FILTER_SANITIZE_STRING)));
if (empty($birth))
{
    header("location: update.php"); // deal with invalid input
    exit();
}

$appearance = ltrim(rtrim(filter_input(INPUT_POST, "appearance", FILTER_SANITIZE_STRING)));
if (empty($appearance))
{
    header("location: update.php");
    exit();
}

$affiliation = ltrim(rtrim(filter_input(INPUT_POST, "affiliation", FILTER_SANITIZE_STRING)));
if (empty($affiliation))
{
    header("location: update.php");
    exit();
}

$pic = ltrim(rtrim(filter_input(INPUT_POST, "pic")));
if (empty($pic))
{
    header("location: update.php");
    exit();
}



/* Include "configuration.php" file */
require_once "configuration.php";



/* Perform query */
$query = "UPDATE characters SET name = :name, age = :age, birth = :birth, appearance = :appearance, affiliation = :affiliation, pic = :pic WHERE id = :id";
$statement = $dbConnection->prepare($query);
$statement->bindParam(":name", $name, PDO::PARAM_STR);
$statement->bindParam(":age", $age, PDO::PARAM_INT);
$statement->bindParam(":birth", $birth, PDO::PARAM_STR);
$statement->bindParam(":appearance", $appearance, PDO::PARAM_STR);
$statement->bindParam(":affiliation", $affiliation, PDO::PARAM_STR);
$statement->bindParam(":pic", $pic, PDO::PARAM_STR);
$statement->bindParam(":id", $id, PDO::PARAM_INT);
$statement->execute();



/* Provide feedback that the record has been modified */
if ($statement->rowCount() > 0)
{
    echo "<p>Record successfully modified.</p>";   
}
else
{
    echo "<p>Record not updated. Either the record does not exist or no changes were made to its details.</p>";
}



/* Provide a link for the user to proceed to a new webpage or automatically redirect to a new webpage */
header("location: " . $siteName . "/php/displayAllRecords.php");
?>        
</body>
</html>